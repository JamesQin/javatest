package com.james.test.net.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * CLASS: MainApp
 *
 * Author: 秦杰
 */
public class MainApp {
	public static void main(String[] args){
		try {
			Socket socket = new Socket("192.168.1.111", 2400);
			if (socket != null && socket.isConnected()) {
				try {
					Thread.sleep(30000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				InputStream is = socket.getInputStream();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
				System.out.println(bufferedReader.readLine());
				bufferedReader.close();
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
