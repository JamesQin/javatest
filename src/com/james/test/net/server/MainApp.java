/**
 * Project Name：JavaNet
 *	File Name：MainApp.java
 *  
 * Description:
 *         a.
 *        	b.
 *			c.
 * Author: 秦杰  Date:2013-11-28 Time:下午5:30:42
 */
package com.james.test.net.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;

/**
 * CLASS: MainApp
 * 
 * Author: 秦杰
 */
public class MainApp {
	private static HashMap<Socket, SocketProcesser> mClients = new HashMap<Socket, SocketProcesser>();

	public static void main(String[] args) {
		new Thread() {
			public void run() {
				try {
					ServerSocket ss = new ServerSocket(2400);
					Socket client = ss.accept();
					if (client != null && client.isConnected()) {
						SocketProcesser t = new SocketProcesser(client);
						mClients.put(client, t);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			};
		}.start();
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		Iterator<Socket> iterator  = mClients.keySet().iterator();
		while(iterator.hasNext()){
			SocketProcesser processer = mClients.get(iterator.next());
			if(processer != null)
				processer.sendMessage("hello");
		}
	}

}
