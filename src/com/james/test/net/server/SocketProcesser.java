/**
 * Project Name：JavaNet
 *	File Name：ProcessThread.java
 *  
 * Description:
 *         a.
 *        	b.
 *			c.
 * Author: 秦杰  Date:2013-11-28 Time:下午5:37:27
 */
package com.james.test.net.server;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * CLASS: ProcessThread
 *
 * Author: 秦杰
 */
public class SocketProcesser{

	private Socket socket ;
	
	/**
	 * @param client
	 */
	public SocketProcesser(Socket client) {
		socket = client;
	}
	
	public void sendMessage(String msg){
		if (socket != null && socket.isConnected()) {
			try {
				BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
				if (bufferedWriter != null) {
					bufferedWriter.write("Hello");
				}
				bufferedWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
