package com.james.test.nio.selector;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by james on 14-4-9.
 */
public class Server {

    private Charset charset = Charset.forName("UTF-8");

    public void init(){
        try {
            //打开一个selector。
            Selector selector = Selector.open();
            InetSocketAddress inetAddress =new InetSocketAddress("127.0.0.1",60000);
            ServerSocketChannel ssc = ServerSocketChannel.open();
            ssc.socket().bind(inetAddress);
            //设置非阻塞模式
            ssc.configureBlocking(false);
            //注册selector
            ssc.register(selector, SelectionKey.OP_ACCEPT);
            while(selector.select() > 0){
                Set keys = selector.selectedKeys();
                Iterator i = keys.iterator();
                while(i.hasNext()){
                    SelectionKey key = (SelectionKey) i.next();
                    selector.selectedKeys().remove(key);
                    if (key.isAcceptable()){
                        SocketChannel sc = ((ServerSocketChannel)key.channel()).accept();
                        sc.configureBlocking(false);
                        sc.register(selector,SelectionKey.OP_READ);
                        key.interestOps(SelectionKey.OP_ACCEPT);
                        String cheetMessage = "欢迎连接服务器：当前时间-》"+new Date().toLocaleString();
                        ByteBuffer bb  = Charset.forName("UTF-8").encode(cheetMessage);
                        sc.write(bb);
                    }
                    if (key.isConnectable()){
                        System.out.println("Client Connected:" + ((SocketChannel) key.channel()).socket().getInetAddress());
                    }
                    if (key.isReadable()){
                        SocketChannel sc = (SocketChannel) key.channel();
                        ByteBuffer bb = ByteBuffer.allocate(1024);
                        String content = "";
                        try{
                            while(sc.read(bb) > 0){
                                bb.flip();
                                content  += charset.decode(bb);
                                bb.clear();
                            }
                            System.out.println("Content:"+content);
                        }catch(Exception e){
                            key.cancel();
                            if (key.channel() != null){
                                key.channel().close();
                            }
                        }
                        String broadMsg = sc.getRemoteAddress().toString()+ "->" + content;
                        if (content.length() > 0){
                            for(SelectionKey sk : selector.keys()){
                                Channel targetChannel = sk.channel();
                                if (targetChannel instanceof SocketChannel){
                                    SocketChannel targetSocketChannel = (SocketChannel) targetChannel;
                                    targetSocketChannel.write(charset.encode(broadMsg));
                                }
                            }
                        }
                    }
                    if (key.isWritable()){
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args){
        new Server().init();
    }
}
