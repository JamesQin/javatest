package com.james.test.nio.selector;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.util.Scanner;

/**
 * Created by james on 14-4-9.
 */
public class Client {

    private Selector selector;

    private Charset charset = Charset.forName("UTF-8");


    public void init() throws  IOException{
        selector = Selector.open();
        try {

            InetSocketAddress remote = new InetSocketAddress("127.0.0.1",60000);
            SocketChannel socketChannel = SocketChannel.open(remote);
            socketChannel.configureBlocking(false);
            socketChannel.register(selector, SelectionKey.OP_READ);

            new ClientThread().start();

            Scanner scanner = new Scanner(System.in);
            while(scanner.hasNext()){
                String line = scanner.nextLine();
                socketChannel.write(charset.encode(line));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args){
        try {
            new Client().init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class ClientThread extends Thread {
        @Override
        public void run() {
            super.run();
            try{
                while(selector.select() > 0 ){
                    for(SelectionKey sk : selector.selectedKeys()){
                        selector.selectedKeys().remove(sk);
                        if (sk.isReadable()){
                            Channel c = sk.channel();
                            SocketChannel sc = (SocketChannel) c;
                            ByteBuffer bb = ByteBuffer.allocate(1024);
                            String content = "";
                            while(sc.read(bb) > 0){
                                bb.flip();
                                content += charset.decode(bb);
                                bb.clear();
                            }
                            System.out.println("聊天信息："+content);
                            sk.interestOps(SelectionKey.OP_READ);
                        }
                    }
                }
            }catch(IOException ex){
                ex.printStackTrace();;
            }
        }
    }
}
