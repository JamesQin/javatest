package com.james.test.nio.charset;

import java.nio.CharBuffer;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.SortedMap;

/**
 * Created by james on 14-4-9.
 */
public class Main {
    public static void main(String args[]){
        //Get charset
        SortedMap<String,Charset> map = Charset.availableCharsets();
        System.out.println("Title:get charset map");
        for(String alias : map.keySet()){
            System.out.println(alias+"-->"+map.get(alias));
        }

        //Encode Decode
        System.out.println("Title:encode + decode");
        Charset charset = Charset.forName("UTF-8");
        CharsetDecoder decoder = charset.newDecoder();
        CharsetEncoder encoder = charset.newEncoder();
        CharBuffer cb = CharBuffer.allocate(3);
        cb.put('秦');
        cb.put('始');
        cb.put('皇');
        cb.flip();
        try {
            ByteBuffer bb = encoder.encode(cb);
            for(int i = 0 ;i < bb.limit(); i++){
                System.out.println(bb.get(i));
            }
            System.out.println(decoder.decode(bb));
        } catch (CharacterCodingException e) {
            e.printStackTrace();
        }

    }
}
