package com.james.test.nio.pipe;

import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.Pipe;

public class Main {

    static class ReadThread extends  Thread{
        private Pipe.SourceChannel mSc = null;
        public ReadThread(Pipe.SourceChannel sc){
            mSc = sc;
        }
        @Override
        public void run() {
            super.run();
            ByteBuffer bb = ByteBuffer.allocate(128);
            try {
                int bytes = mSc.read(bb);
                while(bytes != -1){
                    bb.flip();
                    while(bb.hasRemaining()){
                        System.out.println("Read From Source : "+bb.get());
                    }
                    bb.clear();
                    mSc.read(bb);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static class WriteThread extends  Thread{
        private Pipe.SinkChannel mSc = null;
        public WriteThread(Pipe.SinkChannel sc){
            mSc = sc;
            Socket s ;
        }
        @Override
        public void run() {
            super.run();
            for(long i = 0 ;i < 4096 * 100;i ++){
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ByteBuffer bb = ByteBuffer.allocate(1);
                bb.put((byte)(i%10));
                bb.flip();
                try {
                    System.out.println("write : "+i);
                    mSc.write(bb);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        try {
            Pipe pipe = Pipe.open();
            Pipe.SinkChannel sinkChannel = pipe.sink();
            Pipe.SourceChannel sourceChannel = pipe.source();
            new WriteThread(sinkChannel).start();
            new ReadThread(sourceChannel).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
