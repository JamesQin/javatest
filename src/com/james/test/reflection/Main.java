package com.james.test.reflection;
import javax.annotation.Resource;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Project Name：JavaTest
 *	File Name：Main.java
 *  
 * Description:
 *         a.
 *        	b.
 *			c.
 * Author: 秦杰  Date:2014-1-16 Time:下午8:26:43
 */

/**
 * CLASS: Main
 *
 * Author: 秦杰
 */
public class Main {
	@Deprecated
	private int x;
	@Resource
	private Object o;
	private Integer integer;
	public static void main(String args[]){
		Class<Main> clazz = Main.class;
		Annotation[] annotations = (Annotation[]) clazz.getAnnotations();
		for (Annotation a: annotations) {
			System.out.println(a.toString());
		}
		Field[] fields = clazz.getFields();
		for(Field f: fields){
			System.out.println(f.getName());
		}
	}
}
